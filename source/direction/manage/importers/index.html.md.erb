---
layout: markdown_page
title: "Category Direction - Importers"
description: "This GitLab group is focused on enabling GitLab.com adoption through various importers, mainly migrating GitLab groups and projects by direct transfer and GitHub importer. Find more information here!"
canonical_path: "/direction/manage/importers/"
---

- TOC
{:toc}

## Importers

| | |
| --- | --- |
| Stage | [Manage](/direction/manage/) |
| Maturity | N/A |
| Content Last Reviewed | `2023-02-15` |

### Introduction and how you can help

Thanks for visiting this category direction page on Importers in GitLab. This page belongs to the [Import](https://about.gitlab.com/handbook/product/categories/#import-group) group of the Manage stage and is maintained by the group's Product Manager, [Magdalena Frankiewicz](https://gitlab.com/m_frankiewicz) ([E-mail](mailto:mfrankiewicz@gitlab.com)).

This direction page is a work in progress, and everyone can contribute:
 
- Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Category%3AImporters&first_page_size=100) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?state=opened&page=1&sort=start_date_desc&label_name[]=Category:Importers) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and want to discuss how GitLab can improve Importers, we'd especially love to hear from you.

### Strategy and Themes

<!-- Describe your category. Capture the main problems to be solved in market (themes). Describe how you intend to solve these with GitLab (strategy). Provide enough context that someone unfamiliar with the details of the category can understand what is being discussed. -->

<%= partial("direction/manage/importers/templates/overview") %>

### 1 year plan
<!--
1 year plan for what we will be working on linked to up-to-date epics. This section will be most similar to a "road-map". Items in this section should be linked to issues or epics that are up to date. Indicate relative priority of initiatives in this section so that the audience understands the sequence in which you intend to work on them. 
 -->

<%= partial("direction/manage/importers/templates/next") %>

#### What is next for us
<!-- This is a 3 month look ahead for the next iteration that you have planned for the category. This section must provide links to issues or
or to [epics](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) that are scoped to a single iteration. Please do not link to epics encompass a vision that is a longer horizon and don't lay out an iteration plan. -->
This is a look ahead for the next iteration (about 3 months) that we have planned for the category:

- [In case of issues occurring during migrating by direct transfer, give actionable feedback on what happened and how to solve it](https://gitlab.com/groups/gitlab-org/-/epics/9763).
- [Export and import in batches when migrating GitLab projects by direct transfer](https://gitlab.com/groups/gitlab-org/-/epics/9036).
- [Import GitHub repository collaborators as GitLab project members](https://gitlab.com/gitlab-org/gitlab/-/issues/388716).
- [Provide informative feedback upon finished import from GitHub](https://gitlab.com/groups/gitlab-org/-/epics/9544).
- [Improve experience of choosing GitHub repositories to be imported to GitLab](https://gitlab.com/groups/gitlab-org/-/epics/9430).
        
#### What we are currently working on
<!-- Scoped to the current month. This section can contain the items that you choose to highlight on the kickoff call. Only link to issues, not Epics.  -->
These are highlights from the current month's planned work:

- [Include errors from sub-relations in error messages](https://gitlab.com/gitlab-org/gitlab/-/issues/387782). Errors occurring when migrating GitLab groups and projects are showed to users under import failures, but are not always enough informative. We will include errors from all nested sub-relations to make it very clear why a relation, e.g a Merge Request, failed to be imported. This supports debugging and speeds up resolution time in case of problems.
- [Respond with informative message when migrating by direct transfer is disabled](https://gitlab.com/gitlab-org/gitlab/-/issues/388093). For GitLab group and project migration to work both GitLab instances have to have that feature enabled in application settings. Users trying to initiate import when the feature was disabled, are receiving a 404 error message as a response. We are replacing it with an informative message and indication how to enable the feature.
- [Filter GitHub repos available for import and organise results in tabs](https://gitlab.com/gitlab-org/manage/general-discussion/-/issues/17599). For users with many repos on GitHub it might be difficult to find those that they want to import to GitLab with current filtering option. We are making that search easier by showing repos in three tabs: Owned, Collaborated and Organization. User can further narrow down the search by organization, choosing it from the dropdown.
- [Import GitHub repository collaborators as GitLab project members](https://gitlab.com/gitlab-org/gitlab/-/issues/388716). When importing projects from GitHub to GitLab, repository collaborators are not imported. That resulted in no users having any permissions on a project created in GitLab. To work around it, we are adding users to the GitLab parent group. Now we working on adding all GitHub repository collaborators (that can be mapped to GitLab users) as GitLab project members.
- [Enable re-importing projects](https://gitlab.com/gitlab-org/gitlab/-/issues/23905). Currently it is not possible to import projects from external providers more than once. You cannot import a project that you have access to, if that project had already been imported to GitLab by you or another user. We are working to enable importing projects from external providers many times using different target paths. The re-imported project will not override or merge with previously imported project. External providers include GitHub, Bitbucket Cloud, Bitbucket Server, FogBugz, and Gitea.

#### What we recently completed
<!-- Lookback limited to 3 months. Link to the relevant issues or release post items. -->

* [Released migrating GitLab projects with groups by direct transfer to open beta](https://about.gitlab.com/blog/2023/01/18/try-out-new-way-to-migrate-projects/) *15.8*
* Added a [new application setting](https://docs.gitlab.com/ee/user/admin_area/settings/visibility_and_access_controls.html##enable-migration-of-groups-and-projects-by-direct-transfer') so that GitLab self-managed administrators can more easily enable migrating groups and projects by direct transfer. *15.8*
* Enabled [import of GitHub personal gists via API](https://gitlab.com/gitlab-org/gitlab/-/issues/371099) and [feedback user when a gist cannot be imported](https://gitlab.com/gitlab-org/gitlab/-/issues/376283). *15.8*
* Completed importing GitHub branch protection rules that have an equivalent on GitLab with [translating "Allow specified actors to bypass required pull requests"](https://gitlab.com/gitlab-org/gitlab/-/issues/384939) (see in [documentation](https://docs.gitlab.com/ee/user/project/import/github.html#branch-protection-rules-and-project-settings)). *15.8*

#### What is Not Planned Right Now
<!--  Often it's just as important to talk about what you're not doing as it is to
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand -->

Group Import continuously evaluates and updates the Importers' direction and roadmap. As part of that effort, new Importers such as Trello, CircleCI, Subversion and Azure DevOps (TFS) are being discussed. While these discussions may ultimately lead to the implementation of a new feature or a new Importer, none of them are being planned at this time.

Given our focus on migrating GitLab groups and project by direct transfer and completing the GitHub importer, we are not prioritizing any work that is not in scope of these two efforts. This includes all other importers, as well as issues for GitHub Importer which are not related to our focus area. 

Group Import is not focused on the ability to regularly back up and restore your GitLab data, for example nightly backups of all your data. For more information on this use-case, please see the [Backup and Restore category direction page](https://about.gitlab.com/direction/geo/backup_restore/).

### Best in Class Landscape
<!-- Blanket description consistent across all pages that clarifies what GitLab means when we say "best in class" -->

BIC (Best In Class) is an indicator of forecated near-term market performance based on a combination of factors, including analyst views, market news, and feedback from the sales and product teams. It is critical that we understand where GitLab appears in the BIC landscape.

#### Key Capabilities 
<!-- For this product area, these are the capabilities a best-in-class solution should provide -->

This table provides a quick overview of what GitLab importers exist today and which most important objects they each support. This list is not exhaustive and the detailed information can be found on the [Importers documentation page](https://docs.gitlab.com/ee/user/project/import/).

[tanuki]: https://about.gitlab.com/ico/favicon-16x16.png "GitLab"
[tan2]: <i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:1.25em" aria-hidden="true">

| Import source                                                                                 | Repos       | MRs        | Issues     | Epics     | Milestones | Wiki       | Designs   | API <sup>*</sup> |
|-----------------------------------------------------------------------------------------------|-------------|------------|------------|-----------|------------|------------|-----------|------------|
| [<i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:1.0em" aria-hidden="true"></i> Group and project migration by direct transfer](https://docs.gitlab.com/ee/user/group/import/)           | ✅          | ✅          | ✅          | ✅       | ✅          | ✅         | ✅        | ✅         |
| [<i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:1.0em" aria-hidden="true"></i> Group migration with export files (deprecated)](https://docs.gitlab.com/ee/user/group/settings/import_export.html)     | ➖ | ➖         | ➖          | ✅       | ✅          | ➖         | ➖        | ✅         |
| [<i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:1.0em" aria-hidden="true"></i> Project migration with export files](https://docs.gitlab.com/ee/user/project/settings/import_export.html) | ✅ | ✅         | ✅          | ➖       | ✅          | ✅         | ✅        | ✅         |
| [GitHub](https://docs.gitlab.com/ee/user/project/import/github.html)                          | ✅          | ✅          | ✅          | ➖       | ✅          | ✅         | ➖        | ✅         |
| [Bitbucket Cloud](https://docs.gitlab.com/ee/user/project/import/bitbucket.html)              | ✅          | ✅          | ✅          | ➖       | ✅          | ✅         | ➖        | ❌         |
| [Bitbucket Server](https://docs.gitlab.com/ee/user/project/import/bitbucket_server.html)      | ✅          | ✅          | ❌          | ➖       | ❌          | ➖         | ➖        | ✅         |
| [Gitea](https://docs.gitlab.com/ee/user/project/import/gitea.html)                            | ✅          | ✅          | ✅          | ➖       | ✅          | ➖         | ➖        | ❌         |
| [Git (Repo by URL)](https://docs.gitlab.com/ee/user/project/import/repo_by_url.html)          | ✅          | ✅          | ➖          | ➖       | ➖          | ➖         | ➖        | ❌         |
| [Manifest file](https://docs.gitlab.com/ee/user/project/import/manifest.html)                 | ✅          | ✅          | ➖          | ➖       | ➖          | ➖         | ➖        | ❌         |
| [CSV](https://docs.gitlab.com/ee/user/project/issues/csv_import.html)                         | ➖          | ➖          | ✅          | ➖       | ➖          | ➖         | ➖        | ❌         |
| [FogBugz](https://docs.gitlab.com/ee/user/project/import/fogbugz.html)                        | ➖          | ➖          | ✅          | ➖       | ➖          | ➖         | ➖        | ❌         |

* ✅ : Supported
* ❌ : Not supported
* ➖ : Not applicable

**_<sup>*</sup> This column indicates whether this importer is accessible via API, in addition to the UI._**

#### Roadmap
<!-- Key deliverables we're focusing on to build a BIC solution. List the epics by title and link to the epic in GitLab. Minimize additional description here so that the epics can remain the SSOT. -->

In the 2023 we aim to first of all release migrating projects by direct transfer to general availability (currently it is in open beta) by working to:
- [Further improve tool's documentation](https://gitlab.com/groups/gitlab-org/-/epics/9758).
- [Ensure reliable migrations of large groups and projects](https://gitlab.com/groups/gitlab-org/-/epics/9759).
- [Enable to retry chosen parts of migration, e.g single projects](https://gitlab.com/groups/gitlab-org/-/epics/9760).
- [Provide actionable feedback in case of issues occuring](https://gitlab.com/groups/gitlab-org/-/epics/9763).
- [Show comprehensive migration results](https://gitlab.com/groups/gitlab-org/-/epics/9757).

In pararel we continue the work to obtain the [feature parity and complete maturity of GitHub importer](https://gitlab.com/groups/gitlab-org/-/epics/2984).

After reaching general availbility of GitLab migration by direct transfer, we will work on [migrating users](https://gitlab.com/groups/gitlab-org/-/epics/4616) and importing more of [relevant GitLab resources](https://gitlab.com/groups/gitlab-org/-/epics/9319).

### Target Audience
<!--
List the personas (https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas#user-personas) involved in this category.

Look for differences in user's goals or uses that would affect their use of the product. Separate users and customers into different types based on those differences that make a difference.
-->

- [Delaney, Development Team Lead](https://about.gitlab.com/handbook/product/personas/#delaney-development-team-lead)
- [Allison, Application Ops](https://about.gitlab.com/handbook/product/personas/#allison-application-ops)
- [Dakota, Application Development Director](https://about.gitlab.com/handbook/product/personas/#dakota-application-development-director)

### Automating group and project import with Professional Services

While the long-term goal for the Import group is to provide all the GitLab importing capabilities needed by our customers in our application, we recognize that GitLab's current capabilities may not support specific migration scenarios. Often, we're not aware of these requirements until a large customer provides us with specific migration requirements.

GitLab [Professional Services](https://about.gitlab.com/services/) team uses [Congregate](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate) tool to orchestrate user, group, and project import API calls in order to help customers automate scaled migrations. With the feature of migrating groups and projects by direct transfer ready for production use at any scale, we will be able to substitute the part of Congregate handling migration on groups and projects.

### Deprecations

#### Phabricator task importer

Planned removal: GitLab 16.0, 2023-05-22.

WARNING:
This is a [breaking change](https://docs.gitlab.com/ee/development/deprecation_guidelines/).
Review the details carefully before upgrading.

The [Phabricator task importer](https://docs.gitlab.com/ee/user/project/import/phabricator.html) is deprecated in GitLab 15.7. Phabricator itself as a project is no longer actively maintained since June 1, 2021. We haven't observed imports using this tool. There has been no activity on the open related issues on GitLab.

#### GitLab.com importer

Planned removal: GitLab 16.0, 2023-05-22.

The [GitLab.com importer](https://docs.gitlab.com/ee/user/project/import/gitlab_com.html) is deprecated in GitLab 15.8 and will be removed in GitLab 16.0.
The GitLab.com importer was introduced in 2015 for importing a project from GitLab.com to a self-managed GitLab instance through the UI.
This feature is available on self-managed instances only. [Migrating GitLab groups and projects by direct transfer](https://docs.gitlab.com/ee/user/group/import/#migrate-groups-by-direct-transfer-recommended)
supersedes the GitLab.com importer and provides a more cohesive importing functionality.

See [migrated group items](https://docs.gitlab.com/ee/user/group/import/#migrated-group-items) and [migrated project items](https://docs.gitlab.com/ee/user/group/import/#migrated-project-items) for an overview.

#### Rake task for importing bare repositories

Planned removal: GitLab 16.0, 2023-05-22.

The [Rake task for importing bare repositories](https://docs.gitlab.com/ee/raketasks/import.html) `gitlab:import:repos` is deprecated in GitLab 15.8 and will be removed in GitLab 16.0.

This Rake task imports a directory tree of repositories into a GitLab instance. These repositories must have been
managed by GitLab previously, because the Rake task relies on the specific directory structure or a specific custom Git setting in order to work (`gitlab.fullpath`).

Importing repositories using this Rake task has limitations. The Rake task:

- Only knows about project and project wiki repositories and doesn't support repositories for designs, group wikis, or snippets.
- Permits you to import non-hashed storage projects even though these aren't supported.
- Relies on having Git config `gitlab.fullpath` set. [Epic 8953](https://gitlab.com/groups/gitlab-org/-/epics/8953) proposes removing support for this setting.

Alternatives to using the `gitlab:import:repos` Rake task include:

- Migrating projects using either [an export file](https://docs.gitlab.com/ee/user/project/settings/import_export.html) or
  [direct transfer](https://docs.gitlab.com/ee/user/group/import/#migrate-groups-by-direct-transfer-recommended) migrate repositories as well.
- Importing a [repository by URL](https://docs.gitlab.com/ee/user/project/import/repo_by_url.html).
- Importing a [repositories from a non-GitLab source](https://docs.gitlab.com/ee/user/project/import/).

#### Developer role providing the ability to import projects to a group

Planned removal: GitLab 16.0, 2023-05-22.

WARNING:
This is a [breaking change](https://docs.gitlab.com/ee/development/deprecation_guidelines/).
Review the details carefully before upgrading.

The ability for users with the Developer role for a group to import projects to that group is deprecated in GitLab 15.8 and will be removed in GitLab 16.0. From GitLab 16.0, only users with at least the Maintainer role for a group will be able to import projects to that group.
